const Calculator = require('./lib/calculator')
const waypoints = require('./waypoints.json')

const calculator = new Calculator(waypoints)

console.log('Total distance:', calculator.totalDistance)
console.log('Total duration:', calculator.totalDuration)
console.log('Speeding distance:', calculator.speedingDistance)
console.log('Speeding duration:', calculator.speedingDuration)
