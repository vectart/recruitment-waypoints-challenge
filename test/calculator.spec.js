/* eslint-disable no-unused-expressions */
const expect = require('chai').expect
const Calculator = require('../lib/calculator')

const meter = 0.000008993216059187305

describe('Calculator', () => {
  it('should store waypoints', () => {
    const waypoints = [{}]
    const calculator = new Calculator(waypoints)

    expect(calculator.waypoints).to.equal(waypoints)
  })

  it('should calculate total distance', () => {
    const waypoints = [
      {
        position: {
          latitude: 0,
          longitude: meter * -2
        }
      },
      {
        position: {
          latitude: 0,
          longitude: meter
        }
      },
      {
        position: {
          latitude: 0,
          longitude: meter * 2
        }
      },
      {
        position: {
          latitude: 0,
          longitude: meter * 5
        }
      }
    ]
    const calculator = new Calculator(waypoints)

    expect(calculator.totalDistance).to.equal(7)
  })

  it('should calculate total duration', () => {
    const waypoints = [
      {
        timestamp: '2016-06-21T23:30:00.000Z',
        speed: 10
      },
      {
        timestamp: '2016-06-21T23:40:00.000Z',
        speed: 10
      },
      {
        timestamp: '2016-06-21T23:50:00.000Z',
        speed: 0
      },
      {
        timestamp: '2016-06-22T00:00:00.000Z',
        speed: 0
      },
      {
        timestamp: '2016-06-22T00:10:00.000Z',
        speed: 5
      }
    ]
    const calculator = new Calculator(waypoints)

    expect(calculator.totalDuration).to.equal(1800)
  })

  const speedingWaypoints = [
    {
      timestamp: '2016-06-21T23:30:00.000Z',
      position: {
        latitude: 0,
        longitude: meter * -10
      },
      speed: 0,
      speed_limit: 5
    },
    { // speeding for 5 meters, 5 minutes
      timestamp: '2016-06-21T23:40:00.000Z',
      position: {
        latitude: 0,
        longitude: 0
      },
      speed: 10,
      speed_limit: 5
    },
    { // speeding for 10 meters, 10 minutes
      timestamp: '2016-06-21T23:50:00.000Z',
      position: {
        latitude: 0,
        longitude: meter * 10
      },
      speed: 5,
      speed_limit: 5
    },
    { // speeding for 10 meters, 10 minutes
      timestamp: '2016-06-22T00:00:00.000Z',
      position: {
        latitude: 0,
        longitude: meter * 20
      },
      speed: 10,
      speed_limit: 5
    },
    { // speeding for 10 meters, 10 minutes
      timestamp: '2016-06-22T00:10:00.000Z',
      position: {
        latitude: 0,
        longitude: meter * 30
      },
      speed: 20,
      speed_limit: 5
    },
    { // speeding for 10 meters, 10 minutes
      timestamp: '2016-06-22T00:20:00.000Z',
      position: {
        latitude: 0,
        longitude: meter * 40
      },
      speed: 10,
      speed_limit: 10
    },
    {
      timestamp: '2016-06-22T00:30:00.000Z',
      position: {
        latitude: 0,
        longitude: meter * 50
      },
      speed: 5,
      speed_limit: 5
    }
  ]

  it('should calculate speeding distance', () => {
    const calculator = new Calculator(speedingWaypoints)

    expect(calculator.speedingDistance).to.equal(45)
  })

  it('should calculate speeding duration', () => {
    const calculator = new Calculator(speedingWaypoints)

    expect(calculator.speedingDuration).to.equal(2700)
  })

  it('should be fast enough', () => {
    const start = new Date()

    for (let i = 0; i < 10000; i++) {
      const calculator = new Calculator(speedingWaypoints)
      calculator.totalDuration
      calculator.totalDistance
      calculator.speedingDuration
      calculator.speedingDistance
    }

    const end = new Date()
    expect(end - start).to.be.lessThan(150)
  })
})
