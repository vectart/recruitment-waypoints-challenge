const haversine = require('haversine')

class Calculator {
  constructor (waypoints) {
    this.waypoints = waypoints
  }

  get totalDistance () {
    return this.waypoints.reduce((totalDistance, curWaypoint, i) => {
      if (i === 0) {
        return 0
      }

      const prevWaypoint = this.waypoints[i - 1]
      const distance = Calculator.getDistance(prevWaypoint, curWaypoint)

      return totalDistance + distance
    }, 0)
  }

  get totalDuration () {
    return this.waypoints.reduce((totalDuration, curWaypoint, i) => {
      if (i === 0) {
        return totalDuration
      }

      const prevWaypoint = this.waypoints[i - 1]
      const notDriving = prevWaypoint.speed === 0 && curWaypoint.speed === 0

      if (notDriving) {
        return totalDuration
      }

      const duration = Calculator.getDuration(prevWaypoint, curWaypoint)

      return totalDuration + duration
    }, 0)
  }

  get speedingDistance () {
    return this.waypoints.reduce(Calculator.calculateSpeeding(Calculator.getDistance), 0)
  }

  get speedingDuration () {
    return this.waypoints.reduce(Calculator.calculateSpeeding(Calculator.getDuration), 0)
  }

  static calculateSpeeding (getValue) {
    return (totalValue, curWaypoint, i, waypoints) => {
      if (i === 0) {
        return 0
      }

      const prevWaypoint = waypoints[i - 1]
      const isSpeeding = Calculator.isSpeeding(curWaypoint)
      const wasSpeeding = Calculator.isSpeeding(prevWaypoint)
      let value = 0

      if (isSpeeding || wasSpeeding) {
        if (isSpeeding && wasSpeeding) {
          value = getValue(prevWaypoint, curWaypoint)
        } else {
          const speedLimitWaypoint = Calculator.getSpeedLimitWaypoint(prevWaypoint, curWaypoint)

          if (isSpeeding) {
            value = getValue(speedLimitWaypoint, curWaypoint)
          } else {
            value = getValue(prevWaypoint, speedLimitWaypoint)
          }
        }
      }

      return totalValue + value
    }
  }

  static getDistance (start, end) {
    return haversine(start.position, end.position, { unit: 'meter' })
  }

  static getDuration (start, end) {
    return (Calculator.getTimestamp(end) - Calculator.getTimestamp(start)) / 1000
  }

  static getTimestamp (waypoint) {
    if (typeof waypoint.timestamp === 'string') {
      waypoint.timestamp = Date.parse(waypoint.timestamp)
    }

    return waypoint.timestamp
  }

  static isSpeeding (waypoint) {
    return waypoint.speed > waypoint.speed_limit
  }

  static getSpeedLimitWaypoint (start, end) {
    const duration = Calculator.getDuration(start, end)
    const acceleration = (end.speed - start.speed) / duration
    const accelerationDuration = (start.speed_limit - start.speed) / acceleration
    const breakpoint = accelerationDuration / duration

    if (breakpoint <= 0) {
      return start
    }

    if (breakpoint >= 1) {
      return end
    }

    return {
      timestamp: Calculator.getBetween(Calculator.getTimestamp(start), Calculator.getTimestamp(end), breakpoint),
      position: {
        latitude: Calculator.getBetween(start.position.latitude, end.position.latitude, breakpoint),
        longitude: Calculator.getBetween(start.position.longitude, end.position.longitude, breakpoint)
      }
    }
  }

  static getBetween (a, b, p) {
    return a + ((b - a) * p)
  }
}

module.exports = Calculator
